Born Mayer Huggins
==================
What is it?
-----------

The Born Mayer Huggins potential is 

Equation
--------
.. math::
 
    \phi(r_{ij})=A_{ij}exp(-\frac{r_{ij}}{p_{ij}})+\frac{q_{1}q_{2}}{4\pi\epsilon_{0}r_{ij}}

:math:`\phi`. is the potential energy

:math:`r_{ij}`. is the seperation distance between pairs of atoms i-j

:math:`A`. and :math:`p_{ij}`. are an adjustable parameters

:math:`q_{1}`. and :math:`q_{2}`. are the ion charges

.. code-block:: python
   
    def born_mayer_huggins(A = 1,
        r_ij = np.linspace(1,10,100), 
        p_ij = 1,
        qi = 1.602E-19,
        qj = 1.602E-19 ):
    
        A=1
        r_ij = np.asarray(np.linspace(1,10,100))
        p_ij = 1
        #short range repulsive force
        sr_rep = A*np.exp(-(r_ij)/(p_ij))
        qi = 1.602E-19
        qj = 1.602E-19
        coulomb = np.array([])
        #coulombic interaction force computed from Ewald sum method
        coulomb = (qi*qj)/(4*con.pi*con.epsilon_0*r_ij)
        phi = sr_rep+coulomb

        return r_ij, phi
Plot
----

Examples of use
---------------

When simulating glasses it is important to account for the semi-ionic semi covalent nature. The Born-Mayer-Huggins potential can be used to to represent the "essential structural properties" of silicate or borate glasses. Often times a three body term such as the Stillinger, Weber potential or Feuston and Garofalini potential  will be added to the Born Mayer Huggins to account for bonds such as O-Si-O or Si-O-Si [1]. If a modified or extended three body term is added the potential can take into account directional covalent bonding of oxygen atoms in oxide glasses [3]. When simulating oxide glasses it is important to account for coordination number and correctly modellling short-range order of the glass.


References
----------

[1] Ghalebb, D. (1996). Molecular dynamics simulation of a nuclear waste glass matrix, 37, 232–236.

[2] Delaye, J. ., Louis-Achille, V., & Ghaleb, D. (1997). Modeling oxide glasses with Born–Mayer–Huggins potentials: Effect of composition on structural changes. Journal of Non-Crystalline Solids, 210(2–3), 232–242. http://doi.org/10.1016/S0022-3093(96)00604-7

[3] Chaussedent, S., Teboul, V., & Monteil, A. (2003). Molecular dynamics simulations of rare-earth-doped glasses. Current Opinion in Solid State and Materials Science, 7(2), 111–116. http://doi.org/10.1016/S1359-0286(03)00050-0
