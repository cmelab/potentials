Useful Resources
============

Many Embeddded Atom Potentials
http://www.ctcms.nist.gov/potentials/

Good introduction section to different kinds of interatomic potneitals
LeSar, Richard. Introduction to computational materials science: fundamentals to applications. Cambridge University Press

Fluid potentials
http://molecularsystemsengineering.org/saft.html

Excellent resource for many aspects of nuclear materials, simulations, physical properties, physics, characterization
Konings, Dr. Rudy J. M. et al. Comprehensive Nuclear Materials. United States: Elsevier Ltd., Amsterdam, Netherlands, 2012. Print.

HOOMD-blue molecular dynamics general purpose simulation toolkit pair potential discussion
http://hoomd-blue.readthedocs.io/en/stable/module-md-pair.html

Database of many molecular structures
https://www.ccdc.cam.ac.uk/

Structural and quantum data for many materials and polymorphs
https://materialsproject.org/

List of potentials from LAMPS documentation
http://lammps.sandia.gov/features.html#ff


Review papers/potential comparison papers/good discussion
---------------------------------------------------------

Konings, R. J. M., & Ackland, G. J. (2012). Interatomic Potential Development. Comprehensive Nuclear Materials. Elsevier Inc. http://doi.org/10.1016/B978-0-08-056033-5.00026-4


Silica
******

Good discussion of the physics of simulating glasses and computational artifacts and issues that can occur
Woodcock, L. V., Angell, C. A., & Cheeseman, P. (1976). Molecular dynamics studies of the vitreous state: Simple ionic systems and silica. The Journal of Chemical Physics, 65(4), 1565–1577. http://doi.org/10.1063/1.433213

Ryu, S., & Cai, W. (2008). Comparison of thermal properties predicted by interatomic potential models. Modelling and Simulation in Materials Science and Engineering, 16(8), 85005. http://doi.org/10.1088/0965-0393/16/8/085005

Cook, S. J., & Clancy, P. (1993). Comparison of semi-empirical potential functions for silicon and germanium. Physical Review B, 47(13), 7686–7699. http://doi.org/10.1103/PhysRevB.47.7686

Balamane, H., Halicioglu, T., & Tiller, W. (1992). Comparative study of silicon empirical interatomic potentials. Physical Review B, 46(4), 2250–2279. http://doi.org/10.1103/PhysRevB.46.2250

Uranium
*******

Govers, K., Lemehov, S., Hou, M., & Verwerft, M. (2007). Comparison of interatomic potentials for UO2. Part I: Static calculations. Journal of Nuclear Materials, 366(1–2), 161–177. http://doi.org/10.1016/j.jnucmat.2006.12.070

Govers, K., Lemehov, S., Hou, M., & Verwerft, M. (2008). Comparison of interatomic potentials for UO2. Part II: Molecular dynamics simulations. Journal of Nuclear Materials, 376(1), 66–77. http://doi.org/10.1016/j.jnucmat.2008.01.023

Williams, N. (2014). Atomistic Simulation of Uranium Dioxide Interfaces. Retrieved from http://opus.bath.ac.uk/43950/1/Atomistic_Simulation_of_Uranium_Dioxide_Interfaces.pdf

Reverse Monte Carlo
*******************

McGreevy, R. L. (2001). Reverse Monte Carlo modelling. J.Phys.Condens.Matter, 13(46), R877–R913.

Liquid Crystals
****************

Vroege, G. J., & Lekkerkerker, H. N. W. (1992). Phase transitions in lyotropic liquid crystals: bilayer and micelle stability. Reports on Progress in Physics, 55(July 1991), 1241–1309. http://doi.org/10.1088/0034-4885/55/8/003
