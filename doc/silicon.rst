Silicon based systems
=====================

Bonding Angles
--------------

3 atoms, angle between atoms 180 degrees
4, 120 degrees
5 tetrahedroon 109 degrees
In amorphous silicon not all atoms are 4-fold coordinated.

Simulation methods
------------------

Born-Mayer-Huggins
Tersoff

References
----------
[1] LeSar, Richard. Introduction to computational materials science: fundamentals to applications. Cambridge University Press, 2013.

