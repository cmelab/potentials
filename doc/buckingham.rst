Buckingham
----------

Equation
--------

Buckingham potential

:math:`\phi(r) = Aexp(-Br)-\frac{C}{r^{6}}`

:math:`A,B,C` are constants

.. code-block:: pyhon

    import numpy as np
    import pylab as pl
    import matplotlib.pyplot as plt
def buckingham(A = 4,
	B = 1,
	C = 1,
	r = np.linspace(.8,4,100),
	e = 2.71828
	):
    size = len(r)
    v_Buck = np.ones(size)
  
    for i in range(len(r)):
            v_one = (A)
            v_two = ((e)**((-B)*(r[i])))
            v_three = ((C)/((r[i])**6))
            v_Buck[i] = -((v_one)*(v_two)-(v_three))

    return r, v_Buck
r, v_output=buckingham()
pl.plot(r, v_output)
#x1,x2,y1,y2 = plt.axis()
#plt.axis(x1,x2,y1,y2)
pl.show()


What is it?
-----------

This potential is used to model Pauli repulsion energy and van der Waals energy for the interaction of two unbonded atoms as a function of the intermolecular distance between them.


potential somewhat resembles a lenard jones potential flipped over the X axis

The two terms on the right side of the equation represent repulsion and attraction respectivly.

The first constant ,A,is positive and changes how high the energy barrier rises.

The second constant ,B, is also positive but it should be noted that it is negative in the equation. 
It affects the scale of the graph with smaller numbers making it rise higher above the X axis and extend farther as the distance increases.

The third constnat ,C, is positive 

It also affects the size of the graph but more notably changes the location of the vertical asymatope.


Issues
------

This potential has the risk of an un-physical "Buckingham catastrophe" which is where in charged systems the electrostatic attracion can overcome the repulsive barrier and lead atoms being strongly bonded with a distance of 0

This is about 4 times slower then the lennard jones potential to compute

references
----------

http://www.sklogwiki.org/SklogWiki/index.php/Buckingham_potential

https://en.wikipedia.org/wiki/Buckingham_potential



Coulomb-Buckingham potential

What is it
----------

This version of the potential adds a term to represent electrostatic potential energy. 

:math:`\phi(r) = Aexp(-Br)-\frac{C}{r^{6}}+\frac{q_{1}q_{2}}{4\pi\epsilon_{0}r}`


Examples of Use
---------------

[8] mentions a paper where it is used to model He in UO2,in literature search now only mentioned by other papers not read papers where direct

References
----------

