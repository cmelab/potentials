Stilinger-Weber
===============

What is it?
-----------

The basic structure of the material is built into the potential the method was developed for silicon. Each Si atom is surrounded by 4 other atoms in a tetrahedral shape. The interaction of an atom nad its four nearest neighbors is described by four vectors, which connect the atom to its neighbors. There is a pair potential between the atoms that is dependent on the distance to its neighbors. There are also terms that depend on the bond angles. Determining the angle requires three atoms, the atom in the center and the two atoms to the side of it, 3-body potential. The terms can be found by fitting experimental data. The Stilinger-Weber (SB) potential and similar angular-dependent potentials can provide accurate descriptions of the materials they were described for, but are typically limited to those materials or phases. For example, the (SB) potential describes Si in a perfect tetrahedral solid, but poorly describes it in the liquid or other solid structures, making it difficult to model phase transformations  [1] 

Additional information [2]

Tetrahedral semiconductors Si and Ge shrink when they melt, during melting the coordination number of 4 will increase to an average coordination number of 6. Electrical properties such as the conductivity will be strongly affected by this change [3].

Equation
--------

:math:`r_{1},r_{2},r_{3},r_{4}`. Vectors connecting an atom to its neighbors

:math:`\theta_{jik}`. Angle between the bonds

:math:`r_{ij}.r_{ik}=2r_{ij}r_{ik}cos\theta_{jik}`. Equation to determine the angle between bonds

Three-body interactions

:math:`f_{3}(r_{i},r_{j},r_{k}) = h(r_{ij},r_{ik},\theta_{jik})+h(r_{ji},r_{jk},\theta_{ijk})+h(r_{ki},r_{kj},\theta_{ikj})`

:math:`\theta_{jik}` is the angle between :math:`r_{j}` and :math:`r_{k}`

as long as :math:`r_{ij}` and :math:`r_{ik}` are less than a cutoff and

:math:`h(r_{ij},r_{ik},\theta_{jik}) = \lambda exp[\gamma (r_{ij}-a)^{-1}+\gamma (r_{ik}-a)^{-1}]*(cos \theta_{jik}+\frac{1}{3})^{2}`

The ideal tetrahedral angle gives :math:`cos \theta_{t} = -\frac{1}{3}`

Plot
----
r <a
Uses
----

Model qualitatively condensed phases of silic and quantatative deficieneis

Issues
------

Examples of use
---------------

Used with Born-Mayer-Huggins in [4] to describe the surface of amorphous silica in relaxed, random, ordered configurations



References
----------

[1] LeSar, Richard. Introduction to computational materials science: fundamentals to applications. Cambridge University Press, 2013.
 
[2] http://www.pages.drexel.edu/~cfa22/msim/node41.html

[3] Stillinger, F. H., & Weber, B. (1985). Computer simulation of order in condensed phases of silicon. Phys. Rev. B, 32(8), 5262–5271.

[4] Stallons, J. M., & Iglesia, E. (2001). Simulations of the structure and properties of amorphous silica surfaces. Chemical Engineering Science, 56(14), 4205–4216. http://doi.org/10.1016/s0009-2509(01)00021-5
