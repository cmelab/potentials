Bond-order potentials
=====================

Bond-order potentials are a way to describe dynamic bonding by including the local coordination around an atom. The potential energy is a sum of the atomic energies. There are many varients on this model used for solids, liquids and condensed phase reacting species [1]


Equation
--------

General equation for the energy of each atom in a periodic system as a function of the nearest neighbor seperation [1]

:math:`E_{i} = \frac{1}{2}\sum_{j=1}^{Z_{i}}[qV_{R}(r)+bV_{A}(r)]`

:math:`q` Depends on the local electron density

:math:`V_{R}` Repulsive interaction

:math:`V_{A}` Attractive interaction

:math:`Z` The number of nearest neighbors

:math:`b` Bond order, related to the strength of the chemical bonds. The strength of bonding between nearest neighbor atoms should decrease as the number of nearest neighbors increases as the electrons will be distrubted among a greater number of atoms

An early developer of this method Abell uses :math:`b=cZ^{(-1/2)}`

:math:`E_{i} = \frac{1}{2}\sum_{j=1}^{Z_{i}}[ae^{-\alpha r}-\frac{c}{Z^{1/2}}e^{-\gamma r}]`

As the number of neighbors increases :math:`b` will decrease which will decrease the bond strength and the attractive part of the potential will also be reduced. Defects in the material can cause :math:`b` to change as well which can help to describe local structure

Examples
--------

:ref:`Tersoff Potential <tersoff>`

References
----------
[1] LeSar, Richard. Introduction to computational materials science: fundamentals to applications. Cambridge University Press, 2013.
