Shell Method
===========

What is it?
-----------

Describes atom by splitting it into 2 parts, core of a charge X as the nucleus and core electrons and a shell with all of the valence electrons and no mass which surrounds the core with a charge Y. Harmonic spring constant K links the core and shell. Ions position in the lattice is described by its position in the core and the position of the shell has no physical meaning. Related to polarisability a. [1]
Used to model ionic polarisability. 

Valence atoms are represented by a negatively charged shell connected to positively charged nucleus by a spring at finite temperature the shell center lies away from the nuclear center, giving the ion a net dipole moment and making it polarized. [2]


Issues
------
Does not reproduce defect energies very accurately [1]

References
----------

[1] Williams, N. (2014). Atomistic Simulation of Uranium Dioxide Interfaces. Retrieved from http://opus.bath.ac.uk/43950/1/Atomistic_Simulation_of_Uranium_Dioxide_Interfaces.pdf

[2] Konings, R. J. M., & Ackland, G. J. (2012). Interatomic Potential Development. Comprehensive Nuclear Materials. Elsevier Inc. http://doi.org/10.1016/B978-0-08-056033-5.00026-4
