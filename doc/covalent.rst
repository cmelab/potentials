Covalent Solids/glasses
===============================

Bonding in covalenet materials such as silicon will be localized in certain directions, interatomic potentials must take into account the directional nature of covalent bonds. [1] 

References
----------
[1] LeSar, Richard. Introduction to computational materials science: fundamentals to applications. Cambridge University Press, 2013.

