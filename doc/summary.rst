Summary
=======

Potentials
-----------

Lennard-Jones(LJ)
*****************

* Typically too "stiff"
* Basic pair potential, typically used as starting potential for many types of systems
* Used for Noble gases, rare-gas atoms, many
* Limits many types of systems it does not model well

Mie potential
*************

* Similar to Lennard-Jones but exponentials can change
* can betterr fit certain situations
* Lennard-Jones constants can be used for the Mie potential
* Less steep repulsive term than Lennard-Jones

Morse potential
***************

* Alpha term to govern the shape of the potential
* "soft" repulsive wall
* Sometimes used for interactions in metals or combination with other potentials or methods in metals
* Long range interaction decreases more quickly than van Der Waals forces
* Used to model metals due to the exponential repulsive term.
* In metals can describe some elastic properties

Tersoff
*******

* Variation on bond-order potential
* Used to approximate many different states of silicon, liquid, amorphous, diamond
* Brenner is extended form of Tersoff applied to other systems like germanium and carbon
* Bond order incorporated by local coordination number and angles can differentiate between differenet geometries, pairwise contribution and attractive contribution
* Can be used to model chemival vapor deposition (CVD) for diamond

Stillinger-Weber (SW)
*********************

* Potential for silicon, basic structure of Si built into potential
* Describes interactions with neighboring atoms
* Models tetrahedral well but not other forms
* Many varients exist

Born-Mayer (BM)
***************

* Terms to describe the steepness of the repulsive well
* Repulsive term matches better with electronic distributions of interactions atoms, can have a more physical repulsive term than Lennard-Jones
* Long range van der Waals attraction
* Describes systems with close-shell atoms, ions 
* Often used for ionic systems where electrostatic interactions are included


Born-Mayer-Huggins (BMH)
************************


Embedded-atom model (EAM)
*************************

* Adding a sum of pair potentials to an energy function of the local electron density, motivated by DFT
* Can be used with Morse potential
* Difficult to get parameters and even more difficult for alloys
* Cannot represent dynamic changes in bonding from the local environment changing
* Finnis and Sinclair (EAM)
* No angular terms and does not well describe covalent systems
* Modified embedded-atom method (MEAM) is written in terms of angular dependencies

Shell Model (SM)
****************

* Based on simple harmonic model of polarizability
* Polarization occurs when the electron cloud is not spherically symmetric around the nucleus, creating an instantaneous dipole moment.
* The shell method models a spherical charge held by a harmonical potential (spring) to the nucleus and core electrons.
* The shell method is the basic ionic material approximation with an additional term that represents how much energy is needed to distort the shell, which is assumed to be the harmonic energy.
* The distortion of the shell changes the interactions with neighboring atoms as the core and shell electrons are no longer cenetered in the same place
* The parameters are usually determined by fitting to experimental data
* Anions are usually more tightly bound than cations and are modeled without a shell
* The shell model can be more accurate than a rigid ion model

.. image:: graphs/e_field.jpg

[1]

Types of potentials
-------------------

Pair Potentials
****************

* LJ
* Mie
* BM
* Morse
* The shape of the well influences the bulk modulus and high pressure porperties
* Steepest repulsive region LJ > BM > Mie(10-6) > Morse

Bond-order potentials
*********************

* Tersoff
* Depends on coordination number, local electron density, attractive repulsive interactions
* As coordination number increases the electrons are shared between more atoms and the bond will be weaker
* Describes local environemt and local bonding
* Varients used to describe solids, liquids, condensed phase reacting species
* Si-O-Si is treated differently than O-Si-O, bond order matters

Many Body Potentials
********************

* SW

Volume-dependent potentials
***************************

* Determining the electronic structure of a material requires expensive calculations, volume dependent potentials can be used as an approximation
* A volume dependent term is added to a pair potential sum, as electron density is a function of volume
* Typically have good agreement with bulk, volume-dependent properties, however they are not better than pair potentials for describing defects, grain boundaries, surfaces, dislocations
* Modelling bulk electron gas, a defect will change this density though and will not be taken into account by the model.

Central-force potentials
************************

Angular-dependent potentials
****************************

* SW
* Can be limited to the materials they were developed for


Reactive force potentials
*************************

* Charge-Optimized-Many-Body (COMB)
* Reactive Force Field (ReaxFF)
* Variations on bond-order potentials to model local environment and charge
* Good when atomic charge is adjusted by changing conditions
* Attractive and repulsive energy depend on the charge
* Models different aspects of bonds, torsion, angle, conjugation
* Tries to equilibrate charge, self energy term depends on electronegativity which depends on the change in atomic energy and charge
* Computationally expensive but faster than DFT


Types of Materials
------------------

Ionic Materials
***************

* Typically start with pair potential, add Coulombic long-range interactions
* Ionic materials will have at least two types of atoms Ex(NaCl)
* A potential is needed to describe Na+, Na+ interactions Cl-,Cl- interactions and Na+,Cl- interactions
* Born-Mayer potential is commonly used
* Ionic materials usually depend on polarization
* Potentials need to describe a way to model changes in electronic structure
* The shell model is commonly used to do this*

Covalent Materials
******************

* Bonding is localized so potentials need to represent the directionality of bonds

Silica
******
Can use surface diffusion, rdf, elastic moduli, bond length, lattice constants to verify that model is good

Mixed Systems
*************


Metals
******

* EAM

* The atoms in metals are surrounded by a delocalized "sea" of electrons
* Pair interactions do not provide a good approximation for properties of metals
* Using LJ can get lattice lengths that match experiment at 0K
* If Mie potential is used the thermodynamics of the system can be predicted slightly better, the shape of the short-range repulsion is too strong, the bulk modulus and pressure dependency will still be too large
* The Morse potential works better than most, can be used to describe some elastic properties
* Pair potentials do not work well to describe defects or the electronic density changes from defects

What is being simulated?
------------------------

Point-defects
*************

Surfaces
********

* Diffusion

Grain Boundaries
****************

Dislocations
************

Vacancies
*********

References
----------

[1] LeSar, Richard. Introduction to computational materials science: fundamentals to application        s. Cambridge University Press, 2013.
