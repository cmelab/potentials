Gay-Berne Potential
===================

What is it?
-----------
Developed to describe short-range repulsive and attractive interactions for nonspheerical molecues. Modifications and extensions to the Gay-Berne (GB) potential have applied it to nonidentical uniaxial and biaxial molecules and molecular fragments. It has also been used to study liquid crystals and DNA. It is used to study polar and nonpolar molecular liquids

Examples of Use
---------------

Original (GB) paper notes that long-range elecrostatic interactions need to be considered, examples of including dipole and quadruple moments into liquid crystal and binary mixtures have shown the importance of electrostatic interactions for better describing the physical behavior of materials. [1] Describes a coarse-grained method combining generalized anisotropic uniaxial Gay-Berne potential and multipole expansion in order to study polar and nonpolar molecular liquids, particularly disklike and rodlike molecules. Including electrostatic componenets in the potential has potential to describe, anisotropic electrostatics of hydrogen bonding.


References
----------

[1] Golubkov, P. A., & Ren, P. (2006). Generalized coarse-grained model based on point multipole and Gay-Berne potentials. Journal of Chemical Physics, 125(6). http://doi.org/10.1063/1.2244553
