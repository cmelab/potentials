Morse
=====

Equation
--------

:math:`\phi(r) = \epsilon[e^{-2\alpha(r-r_{m})}-2e^{-\alpha(r-r_{m})}]`

:math:`r_{m}` is the position of the well minimum
:math:`\epsilon` is the well depth
:math:`\alpha` determines the shape of the potential well
