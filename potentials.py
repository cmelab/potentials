from __future__ import division
import math as m
import scipy
from scipy import constants as con
import numpy as np
def born_mayer_huggins(A = 13.40E-8,
        r_ij = np.linspace(0.05,3,100), 
        p_ij = 0.29,
        qi = 1.602E-19,
        qj = 1.602E-19 ):

    #Phi(r)
    #Adjustable parameter
    #r_ij is the seperation distance between pairs of atoms i-j
    #p_ij is an adjustable parameter
    #short range repulsive force
    sr_rep = A*np.exp(-(r_ij)/(p_ij))
    #qi and qj are the charges of the atoms
    qi = 1.602E-19
    qj = 1.602E-19
    #coulombic iteraction forces calculated from Ewald sum method
    coulomb = np.array([])
    coulomb = (qi*qj)/(4*con.pi*con.epsilon_0*r_ij)
    #potential energy phi
    phi = sr_rep+coulomb

    return r_ij, phi

def lennard_jones(epsilon = 10,
        sigma = 0.5,
        r = np.linspace(0.48,2,100),
        alpha = 1,
        r_cut = 3
        ):
    size = len(r)
    v_lj = np.ones(size)
    
    for i in range(len(r)):
        if r[i] < r_cut:
            v_att = ((sigma)/(r[i]))**12
            v_rep = alpha*((sigma)/(r[i]))**6
            v_lj[i] =  (4*epsilon*(v_att - v_rep))
        elif i >= r_cut:
            v_lj[i] = 0
    
    return r, v_lj

class Tersoff():
    def __init__(self,r,A,B,lambda1,lambda2,beta1,n,c,d,h,R,D,zeta1):
        self.r = r
        self.A = A
        self.B = B
        self.lambda1 = lambda1
        self.lambda2 = lambda2
        self.beta1 = beta1
        self.n = n
        self.c = c
        self.d = d
        self.h = h
        self.R = R
        self.D = D
        self.zeta1 = 1
        self.size = len(self.r)
        self.fR = self.fR_(self.A,self.lambda1,self.r,self.size)
        self.fA = self.fA_(self.B,self.lambda2,self.r,self.size)
        self.fC = self.fC_(self.r,self.R,self.D,self.size)
        self.b = self.b_(beta1,n,zeta1)
        
        return

    def test(self):
        test = self.fC*(self.fR+self.b*self.fA)
        print(test)
        return
    #def potential(self,fC,fR,b,fA)
        
     #   v = self.fC(r)*(self.fR(self.r,self.R,self.D)+self.b)


    def fR_(self,A,lambda1,r,size):
        fR = np.ones([size])
        for i in range(len(r)):
            fR[i] = A*np.exp(-lambda1*r[i])
        return fR

    def fA_(self,B,lambda2,r,size):
        fA = np.ones([size])
        for i in range(len(r)):
            fA[i] = B*np.exp(-lambda2*r[i])
        return fA
    
    def fC_(self,r,R,D,size):
        #print(r)
        fC = np.ones([size])
        for i in range(len(r)):
            if r[i] < R-D:
                fC[i] = 1
            elif R-D < r[i] < R+D:
                #print('flag1')
                fC[i] = (1/2)-((1/2)*np.sin((np.pi/2)*((r[i]-R)/(D))))
                #print(fC[i])
            elif r[i] > R+D:
                fC[i] = 0
        #print('flag')
        return fC

    def b_(self,beta1,n,zeta1):
    
        b = (1)/(1+beta1**(n*zeta1**(n)))**(1/(2*n))

        return b
tersoff = Tersoff(r = np.linspace(0.5,2,10),
        A = 1830.8,
        B = 471.18,
        lambda1 = 2.4799,
        lambda2 = 1.7322,
        beta1 = 1.1E-6,
        n = 0.78734,
        c = 1.0039E5,
        d = 16.217,
        h = -0.59825, # reading as negative?
        R = 1.95,
        D = 0.15,
        zeta1=1)
tersoff.test()

def morse(a,
        r,
        r_m,
        epsilon1):
    
    size = len(r)
    phi =  np.ones([size])
    for i in range(len(r)):
        term1 = np.exp(-2*a*(r[i]-r_m))
        term2 = 2*np.exp(a*(r[i]-r_m)) 
        phi[i] = epsilon1*(term1-term2)
   
    return r,phi

class SW():

    def __init__(self,a,A,B,p,q,lamda1,gamma,r,r1,r2,r3,epsilon):
        '''a is cut off term x is a term to keep r3 constant and modify other values, not part of the potential'''
        self.a = a
        self.A = A
        self.B = B
        self.p = p
        self.q = q
        self.lamda1 = lamda1
        self.gamma = gamma
        self.r = r
        self.r1 = r1
        self.size = len(r)
        self.size1 = len(r1)
        self.r2 = r2
        self.r3 = r3
        self.epsilon = epsilon

        
                
        self.theta1 = self.compute_theta(self.r1,self.r2,self.r3)
        self.theta2 = self.compute_theta(self.r2,self.r1,self.r3)
        self.theta3 = self.compute_theta(self.r3,self.r1,self.r2)
        
        self.h1 = self.h(self.r1,self.r2,self.theta3)
        self.h2 = self.h(self.r2,self.r3,self.theta1)
        self.h3 = self.h(self.r3,self.r1,self.theta2)
    
        self.f3 = self.three_body(self.h1,self.h2,self.h3)
        self.f2 = self.two_body(self.r)
        self.v = self.f3*self.epsilon
        return


    def h(self,r1,r2,theta):
        for i in range(len(theta)):
            #print('theta[i] is deg', theta[i])
            theta[i] = np.deg2rad(theta[i])
            #print('theta in rad', theta[i])
        h = np.ones(self.size1)
        for i in range(self.size1):
            #temp = (np.cos(np.rad2deg(theta[i]))+(1/3))**(2)

            #print('temp is ',temp)   
            h[i] = self.lamda1*np.exp(self.gamma*(r1[i]-self.a)**(-1)+self.gamma*(r2[i]-self.a)**(-1))*(np.cos(theta[i])+(1/3))**(2)
            #print('h',h[i])

        return h
    
    def two_body(self,r):
        f2 = np.ones(len(r))
        for i in range(len(r)):
            if r[i] >= self.a:
                f2[i] = 0
            elif r[i] < self.a:
                f2[i] = self.A*((self.B*r[i]**(-self.p)-r[i]**(-self.q))*np.exp((r[i]-self.a)**(-1)))
        return f2
    
    def three_body(self,h1,h2,h3):
        f3 = np.ones(self.size1)
        for i in range(self.size1):            
            f3[i] = h1[i]+h2[i]+h3[i]
        np.set_printoptions(precision=2)
        #float_formatter = lambda x: "%.2f" % x

        #print('f3 is ',f3)
        return f3
    
    def compute_theta(self,r1,r2,r3):
        '''Using law of cosines, returns angle across the triangle from side r1'''   
        theta = np.ones(len(r1))         
        for i in range(len(r1)):              
            if r1[i]+r2[i] > r3[i] and r2[i]+r3[i] > r1[i] and r1[i]+r3[i] > r2[i]:
                x = (((-r1[i]**2)+(r2[i]**2)+(r3[i]**2))/(2*r2[i]*r3[i])) 
                theta[i] = np.arccos(x)
                theta[i] = np.rad2deg(theta[i])  
            else: 
                theta[i] = 0
        #print('r1',r1[i],'r2',r2[i],'r3',r3[i],'theta',theta[i])
        return theta

def yukawa(r,g,lamda1):
    
    size = len(r)
    J = np.ones(size)
    for i in range(size):
        J[i] = -(g)**2*((np.exp(-lamda1*r[i]))/(r[i]))

    return r, J


#class GB():

   # def __init__():

    #    def chi_():
     #   epsilon_s**(1/u)-epsilon_e**(1/u))/(epsilon_s**(1/u)+epsilon_e**(1/u))

      #  return
    #def epsilon_(self):
     #   1-(chi_/2)*(((np.dot(r,u1)+np.dot(r,u2))**2/(1+chi_*(np.dot(u1,u2))))+((np.dot(r,u1)-np.dot(r,u2))**2/(1-chi_*(np.dot(u1,u2)))))

    #def sigmaLG(self):
     #   sigma0*

      #  return

def dzugutov(self,c,a,r,A,B.m,d):
    size = len(r)
    V1 = np.ones(size)
    V2 = np.ones(size)
    V = np.ones(size)
    for i in range(size):
        if r<a:
            V1[i] = A*(r**(-m)-B)*np.exp((c)/(r-a))

        elif r>=a
            V1[i] = 0
        if r < b:
            V2[i] = B*np.exp((d)/(r-b))
        elif r >=b:
            V2[i] = 0
    V[i] = V1[i]+V2[i]
    return V
