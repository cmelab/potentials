.. potentials documentation master file, created by
   sphinx-quickstart on Thu Jun 15 12:33:38 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to potentials's documentation!
======================================
Potentials:

.. toctree::
    :titlesonly:
   
    doc/born_mayer_huggins
    doc/tersoff 
    doc/weber
    doc/lennard_jones
    doc/morse
    doc/buckingham
    doc/bks
    doc/zbl

Potentials Used in Hoomd

.. toctree::
    :titlesonly:

    doc/tersoff
    doc/lennard_jones
    doc/morse
    doc/zbl
    doc/yukawa
    doc/dpd
    doc/lj_force
    doc/gauss
    doc/gb
    doc/mie
    doc/moliere
    doc/onsager


Materials/Bonding types/Methods/General:

.. toctree::
    :titlesonly:

    doc/shell   
    doc/summary
    doc/covalent
    doc/ionic
    doc/metals
    doc/rmc
    doc/ewald
    doc/silicon
    doc/bond_order

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

