import hoomd;
from hoomd.md import nlist as nl # to avoid naming conflicts
import hoomd.metal
import hoomd.deprecated
from hoomd.md import *
import hoomd.data as data
import numpy as np
import numpy.random as rd
# parameters of the unit/super cell

def init_velocity(n, temp):
    v = rd.random((n,3))
    v -= 0.5
    meanv = np.mean(v,0)
    meanv2 = np.mean(v**2,0)
    #fs = np.sqrt(3.*temp/meanv2)
    fs = np.sqrt(temp/meanv2)
    #print('scaling factor:{}'.format(fs))
    #print('v0:{}'.format(v))
    v = (v-meanv) #shifts the average velocity of the simulation to 0
    v *= fs  #scaling velocity to match the desired temperature
    return v

def initialize_snapshot_T(snapshot, temp):
    v = init_velocity(snapshot.particles.N,temp)
    snapshot.particles.velocity[:] = v[:]
    return snapshot

hoomd.context.initialize("")
# initialize a simple fcc array of particles

#snapshot = data.make_snapshot(N=100, particle_types = ['Al'], box = data.boxdim(L=a))

system = hoomd.init.create_lattice(unitcell=hoomd.lattice.fcc(a=3.597),
        n = 9)
snapshot = system.take_snapshot()
snapshot.particles.types = ['Cu']
snapshot = initialize_snapshot_T(snapshot, 1)
system.restore_snapshot(snapshot)
print(snapshot.particles.types)
#snapshot.particles.types = ['Al']
#snapshot.particles.group[0] = [0]
#snapshot = system.take_snapshot()
#snapshot.particles.types[0] = ['Al']
#snapshot.particles.types = [0]
#system.restore_snapshot(snapshot)
#hoomd.init.read_snapshot(snapshot)
nl = hoomd.md.nlist.cell();
#system = hoomd.deprecated.init.create_empty(N=num,phi_p = None, name = "Al", box=data.boxdim(L=a), particle_types=['Al'])

#p1 = (-0.5*a, -0.5*a, -0.5*a)
#p2 = (-0.5*a,  0.0*a,  0.0*a)
#p3 = ( 0.0*a, -0.5*a,  0.0*a)
#p4 = ( 0.0*a,  0.0*a, -0.5*a)
#plist = [p1, p2, p3, p4]

# positiona

#for i,p in enumerate(system.particles):
#    p.position = plist[i]
#    p.type = "Al"

# replicate
#system.replicate(nx=8, ny=8, nz=8)

# simple eam potential

hoomd.metal.pair.eam("Cu1.eam.fs","FS",nl)
#nl_c = nlist.set_params(r_buff = 2.0, check_period=10)

# integrate forward in the nve ensemble
integrate.mode_standard(dt=1e-4)
integrate.nvt(group=hoomd.group.all(), kT = 5, tau = 1.0)

# run for 500 steps
log = hoomd.analyze.log(filename='fccCu.log', quantities=['lx', 'ly', 'lz','potential_energy','pressure', 'num_particles'], period=1e2, overwrite = True)
hoomd.dump.gsd("Cu.gsd", period=1e3, group = hoomd.group.all(), overwrite=True);
hoomd.run(1e3)
