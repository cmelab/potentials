import mbuild as mb
from mbuild.utils.io import get_fn
file_name = 'Li2O'
file_path = '/Users/mitchellleibowitz/Desktop/git_repositories/potentials/simulations/mod_buckingham/'
#Li2O.name ='Li2O'
#filled = mb.fill_box(Li2O,n_compounds = 500, box = []
angle_values = [90,90,90]
spacing = [4.61,4.61,4.61]
basis ={'O' :[[0., 0., 0.]], 'Li' : [[0.5, 0.5,0.5]]}
lio2_lattice = mb.Lattice(spacing,angles=angle_values,lattice_points = basis)
li2O = mb.Compound()
li2O = mb.load(file_path+file_name+'.pdb')
basis_dictionary = {'Li2O' : li2O}
#lattice_vector = [[],[],[]]
expanded_cell= lio2_lattice.populate(x=5, y =5, z =5, compound_dict = basis_dictionary) 

expanded_cell.save('../mod_buckingham/'+file_name+'.hoomdxml')




