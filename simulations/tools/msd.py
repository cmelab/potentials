import scipy
import os
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1
    f.close()
    
tau = 2.15144e-12
dt = 1e-5
sigma = 3.4 #angstroms
file_name = '0_786_T_0_5msd.log'
file_place = str(os.path.abspath('../lj/'+file_name))
data = np.genfromtxt(file_place)
time=data[1000:,0]#*tau*dt
msd=data[1000:,1]#*sigma**2
slope, intercept, r_value, p_value, std_err = stats.linregress(time,msd)
#plt.xlim((0,2.5e-11))
plt.plot(time,msd)
plt.show()
print('time is',time[5000],'msd is',msd[5000])
print('r squared is',r_value**2)
print('slope is',slope,'intercept is',intercept)
