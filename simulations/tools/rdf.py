import hoomd
import hoomd.md
import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline
from freud import parallel, box, density
import gsd
import gsd.hoomd
import gsd.fl
parallel.setNumThreads(4)

file_path = '../mod_buckingham/mod_buckingham_Li2O_1750'
#
#file name, average rdf over last 'frames' number of frames, with a r_cut of rmax and a plot label
def plot_rdf(file, frames, rmax, label):
    # frames is the number of frames you want to average over starting from
    # the last frame
    rdf = density.RDF(rmax=rmax, dr=0.05)
    f = gsd.fl.GSDFile(file, 'rb')
    t = gsd.hoomd.HOOMDTrajectory(f)
    for i in range(1, frames):
        sim_box = t[-i].configuration.box
        fbox = box.Box(Lx=sim_box[0], Ly=sim_box[1], Lz=sim_box[2])
        pos = t[-i].particles.position
        rdf.accumulate(fbox, pos, pos)
                       
    r_avg = np.copy(rdf.getR())
    y_avg = np.copy(rdf.getRDF())
    plt.plot(r_avg, y_avg, label=label)
    plt.ylabel("g(r)")
    plt.xlabel("r")


# Here we will read in the log file
data = np.genfromtxt(fname=file_path+'.log', skip_header=True)

# Now we use the first col as the x and the 2nd col as the y
plt.plot(data[:,0], data[:,1])

plt.xlabel('time step')
plt.ylabel('potential_energy')
plt.show()

# Plots the average rdf over the last 10 frames, with an r_cut of 5.0
plot_rdf(file_path+'.gsd', 100, 10.0, "kT = 1750")
