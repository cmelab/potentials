import pygame
import random
import math
#import numpy as np
background_colour = (0,255,255)
(width, height) = (1900, 1000)


def addVectors(angle1, length1, angle2, length2):
    x = math.sin(angle1) * length1 + math.sin(angle2) * length2
    y = math.cos(angle1) * length1 + math.cos(angle2) * length2

    angle = 0.5 * math.pi - math.atan2(y, x)
    length = math.hypot(x, y)

    return (angle, length)

def collide(p1, p2):
    dx = p1.x - p2.x
    dy = p1.y - p2.y

    dist = math.hypot(dx, dy) 
    if dist < p1.size + p2.size:
        angle = math.atan2(dy, dx) + .5 * math.pi
        total_mass = p1.mass + p2.mass
   
        if p1.mass != p2.mass:
            (p1.angle, p1.speed) = addVectors((p1.angle), (p1.speed*(p1.mass-p2.mass))/total_mass,(angle), (2*p2.speed*p2.mass)/total_mass)
            (p2.angle, p2.speed) = addVectors(p2.angle, p2.speed*(p2.mass-p1.mass)/total_mass, angle+math.pi, 2*p1.speed*p1.mass/total_mass)
        else:
            (p1.angle, p1.speed) = addVectors(p1.angle, p1.speed*.5, angle, (2*p2.speed*p2.mass)/total_mass)
            (p2.angle, p2.speed) = addVectors(p2.angle, p2.speed*.5, angle+math.pi, 2*p1.speed*p1.mass/total_mass)
        p1.speed *=.7
        p2.speed *=.7

        overlap = .5*(p1.size + p2.size - dist+1)
        p1.x += math.sin(angle)*overlap
        p1.y -= math.cos(angle)*overlap
        p2.x -= math.sin(angle)*overlap
        p2.y += math.cos(angle)*overlap


def collide2(q1, p1):
    dx = p1.x - q1.x
    dy = p1.y - q1.y

    dist = math.hypot(dx, dy) 
    if dist < p1.size + q1.size:
        angle = math.atan2(dy, dx) + .5 * math.pi
        total_mass = p1.mass + q1.mass
   
        if p1.mass != q1.mass:
            (p1.angle, p1.speed) = addVectors((p1.angle), (p1.speed*(p1.mass-q1.mass))/total_mass,(angle), (2*q1.speed*q1.mass)/total_mass)
            (q1.angle, q1.speed) = addVectors(q1.angle, q1.speed*(q1.mass-p1.mass)/total_mass, angle+math.pi, 2*p1.speed*p1.mass/total_mass)
        else:
            (p1.angle, p1.speed) = addVectors(p1.angle, p1.speed*.5, angle, (2*q1.speed*q1.mass)/total_mass)
            (p2.angle, p2.speed) = addVectors(q1.angle, q1.speed*.5, angle+math.pi, 2*p1.speed*p1.mass/total_mass)
        p1.speed *=.5
        q1.speed *=.8

        overlap = .5*(p1.size + q1.size - dist+1)
        p1.x += math.sin(angle)*overlap
        p1.y -= math.cos(angle)*overlap
        q1.x -= math.sin(angle)*overlap
        q1.y += math.cos(angle)*overlap



    
def bond(p1, p2):
    dx = (p1.x - p2.x)
    dy = (p1.y - p2.y)
    dist = math.hypot(dx, dy)

    theta = math.atan2(dy, dx)
    force = (-1*((((10000)*(2.71828**(-.12*dist))-(1/(dist**6)))*.1)))+.01
    (p1.angle, p1.speed)=addVectors(p1.angle, p1.speed, theta - 0.5*math.pi, force/p1.mass)
    (p2.angle, p2.speed)=addVectors(p2.angle, p2.speed, theta + 0.5*math.pi, force/p2.mass)

class Particle():
    def __init__(self, x, y, size, mass=1):
        self.x = x
        self.y = y
        self.size = size
        self.mass = mass
        self.colour = (255, 0, 0)
        self.thickness = 3
        self.speed = 0
        self.angle = 0


    def display(self):
        pygame.draw.circle(screen, self.colour, (int(self.x), int(self.y)), self.size, self.thickness)

    def move(self):
        self.x += math.sin(self.angle) * self.speed
        self.y -= math.cos(self.angle) * self.speed 
        self.speed *= .99

    def accelerate(self, vector):
        (self.angle, self.speed) = addVectors(self.angle, self.speed,)
        

    def bounce(self):
        if self.x > width - self.size:
            self.x = 2*(width - self.size) - self.x
            self.angle = - self.angle

        elif self.x < self.size:
            self.x = 2*self.size - self.x
            self.angle = - self.angle

        if self.y > height - self.size:
            self.y = 2*(height - self.size) - self.y
            self.angle = math.pi - self.angle

        elif self.y < self.size:
            self.y = 2*self.size - self.y
            self.angle = math.pi - self.angle

class Neutron():

    def __init__(self, x, y, size, mass=1):
        self.x = x
        self.y = y
        self.size = size
        self.mass = mass
        self.colour = (255, 0, 0)
        self.thickness = 3
        self.speed = 0
        self.angle = 0


    def display(self):
        pygame.draw.circle(screen, self.colour, (int(self.x), int(self.y)), self.size, self.thickness)

    def move(self):
        self.x += math.sin(self.angle) * self.speed
        self.y -= math.cos(self.angle) * self.speed
        self.speed *= 1.01

    def bounce(self):
        if self.x > width - self.size:
            self.x = 2*(width - self.size) - self.x
            self.angle = random.uniform(0, 6.2832)
            self.speed = random.randint(20, 50)

        elif self.x < self.size:
            self.x = 2*self.size - self.x
            self.angle = random.uniform(0, 6.2832)
            self.speed = random.randint(20, 50)

        if self.y > height - self.size:
            self.y = 2*(height - self.size) - self.y
            self.angle = random.uniform(0, 6.2832)
            self.speed = random.randint(20, 50)

        elif self.y < self.size:
            self.y = 2*self.size - self.y
            self.angle = random.uniform(0, 6.2832)
            self.speed = random.randint(20, 50)

screen = pygame.display.set_mode((width, height))
pygame.display.set_caption('radiation_simulator')


number_of_neutrons = 1
my_neutrons = []

for n in range(1, 2):
    size = random.randint(5, 5)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 1

    neutron = Neutron(1000, 1000, size, mass)
    neutron.speed = random.randint(50, 50)
    neutron.angle = random.uniform(0, 6.2832)
  
    my_neutrons.append(neutron)

number_of_particles = 2
my_particles = []

# http://www.mathopenref.com/coordpolycalc.html
# use this to generate cordinates for shapes


for n in range(11, 12):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(250, 300, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(1, 2):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(250, 250, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(2, 3):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(250, 200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(3, 4):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(200, 300, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(4, 5):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5
    particle = Particle(200, 250, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(5, 6):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(700, 700, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(7, 8):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(150,  300, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(8, 9):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1500, 250, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(9, 10):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(300, 900, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(10, 11):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(300, 600, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(11, 12):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(12, 13):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(13, 14):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(15, 16):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)
for n in range(14, 15):
    size = random.randint(20, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    mass = 5

    particle = Particle(1000, 1200, size, mass)
    particle.speed = random.randint(0, 10)
    particle.angle = random.uniform(0, math.pi*2)

    my_particles.append(particle)

selected_particle = None
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    screen.fill(background_colour)

    for i,  neutron in enumerate(my_neutrons):
        neutron.move()
        neutron.bounce()
        for particle in my_particles[i+1:]:
            collide2(neutron, particle)
        neutron.display()
    for i,  particle in enumerate(my_particles):
        particle.move()
        particle.bounce()
        for particle2 in my_particles[i+1:]:
            collide(particle, particle2)
            bond(particle, particle2)
        particle.display()

    pygame.display.flip()
    clock = pygame.time.Clock()
    clock.tick(120)
