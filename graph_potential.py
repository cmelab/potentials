import matplotlib.pyplot as plt
import potentials as p
from potentials import *
r_ij, phi = p.born_mayer_huggins()
plt.figure()
plt.plot(r_ij,phi)
plt.axis('auto')
plt.show()



#plt.plot([1,2,3,4], [1,4,9,16], 'ro')
#plt.axis([0, 6, 0, 20])
#plt.show()
