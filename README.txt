Google_doc_potentials can also be found at https://drive.google.com/open?id=0B1p7paj4DRi-NEpDQkpueGJTWGc

Simulation_Results.docx results of simulations run in hoomd_notebooks.ipynb. Describes current status of diffusion coefficient for LJ for Liquid Argon from Rahman1964, Melting temperature for modified Buckingham for LiO2 from Oda 2007 and EAM for Cu from https://www.ctcms.nist.gov/potentials/Download/Cu-MIM/Cu1.eam.fs
# For loop in snapshot change mass
#M.I. Mendelev (2008). 
*Use the cell called EAM attempt 2

Simulation_results.xlsx Organization for Simulation_Results.docx

Story.docx brief note on Ionic materials and Helium

Force_Fields.xlsx Organization, LJ or not, polarizable or not for force fields

paper_organization.docx Brief notes what each section needs

Materials.xlsx  Organize paper by material?

Motivation.xlsx Organization

Chronology.xlsx Organization, when potentials were developed

Chart_of_how_potentials_connect.xlsx Making a distinction between LJ type, r^ power, exp type, and Z1,Z2 or other mathematical form

Early Literature Review Early notes, still trying to define project, possible simulation of nuclear materials, different types of silicon, fiber optic sensors, determine what kinds of potentials exist for what types of systems

Mendeley

mitch_nuclear_metals ~400 papers on simulations, also original papers for many simulations

paper laTex paper information, slightly older version of the paper can also be found at https://www.overleaf.com/10363348mnfjxbdmrcjy

simulations simulation information EAM folder contains metal_test.py should be a similar version to what is on hoomd_notebooks for EAM. EAM for copper requires https://www.ctcms.nist.gov/potentials/Download/Cu-MIM/Cu1.eam.fs
#M.I. Mendelev (2008). 
https://www.ctcms.nist.gov/potentials/Cu.html
Trying to reproduce predicted melting temperature
lj trying to reproduce diffusion coefficient folder, Lennard Jones results, MSD.log files for diffusion coefficient for Rahman1964

Mod_buckingham trying to verify melting temperature modified buckingham potential for LiO2 for Oda 2007

tools tools to analyze simulations
msd.py get mean squared displacement 
pdb_xml.py convert pdb molecule file to xml for hoomd to read
plotly_Test.py MSD plotting test
rdf.py untested should get rdf out to verify melting temperatures

